package org.example;

import org.junit.Before;
import org.junit.Test;

import java.util.Arrays;

import static org.junit.Assert.*;

public class GameOfLifeTests {

    private GameOfLife gameOfLife;

    @Before
    public void initVoidGrid(){
        int[][] grid = {
                { 0, 0, 0, 0, 0 },
                { 0, 0, 0, 1, 1 },
                { 0, 0, 0, 0, 1 },
                { 0, 0, 1, 1, 0 },
                { 1, 0, 1, 1, 1 },
        };
        gameOfLife = new GameOfLife(grid.length, grid[0].length, grid);
    }

    // Rule 1 - case true
    @Test
    public void whenCellIsAliveAndHasLessThanTwoAliveNeighbours_thenTrue() {
        assertTrue(gameOfLife.isCellAliveAndHasLessThanTwoLiveNeighbours(4,0));
    }

    // Rule 1 - case false
    @Test
    public void whenCellIsDeadAndHasLessThanTwoAliveNeighbours_thenFalse() {
        assertFalse(gameOfLife.isCellAliveAndHasLessThanTwoLiveNeighbours(0,3));
    }

    // Rule 2 - case true
    @Test
    public void whenCellIsAliveAndHasMoreThanThreeAliveNeighbours_thenTrue() {
        assertTrue(gameOfLife.isCellAliveAndHasMoreThanThreeLiveNeighbours(4, 3));
    }

    // Rule 2 - case false
    @Test
    public void whenCellIsDeadAndHasMoreThanThreeAliveNeighbours_thenFalse() {
        assertFalse(gameOfLife.isCellAliveAndHasMoreThanThreeLiveNeighbours(3, 4));
    }

    //Rule 3
    @Test
    public void whenCellIsAliveAndHasTwoAliveNeighbours_thenTrue() {
        assertTrue(gameOfLife.isCellAliveAndHasTwoOrThreeLiveNeighbours(1, 3));
    }

    //Rule 3
    @Test
    public void whenCellIsAliveAndHasThreeAliveNeighbours_thenTrue() {
        assertTrue(gameOfLife.isCellAliveAndHasTwoOrThreeLiveNeighbours(2, 4));
    }

    //Rule 3
    @Test
    public void whenCellIsDeadAndHasTwoAliveNeighbours_thenFalse() {
        assertFalse(gameOfLife.isCellAliveAndHasTwoOrThreeLiveNeighbours(0,3));
    }

    //Rule 3
    @Test
    public void whenCellIsDeadAndHasThreeAliveNeighbours_thenFalse() {
        assertFalse(gameOfLife.isCellAliveAndHasTwoOrThreeLiveNeighbours(3, 1));
    }

    // Rule 4 - case true
    @Test
    public void whenCellIsDeadAndHasThreeAliveNeighbours_thenTrue() {
        assertTrue(gameOfLife.isCellDeadAndHasThreeLiveNeighbours(3, 1));
    }

    // Rule 4 - case false
    @Test
    public void whenCellIsAliveAndHasThreeAliveNeighbours_thenFalse() {
        assertFalse(gameOfLife.isCellDeadAndHasThreeLiveNeighbours(3, 3));
    }

    @Test
    public void whenCellHasZeroLivingNeighbours_thenReturnZero() {
        assertEquals(0, gameOfLife.countAliveNeighbours(1, 1)) ;
    }

    @Test
    public void whenCellHasOneLivingNeighbour_thenReturnOne() {
        assertEquals(1, gameOfLife.countAliveNeighbours(1, 2)) ;
    }

    @Test
    public void whenCellHasTwoLivingNeighbours_thenReturnTwo() {
        assertEquals(2, gameOfLife.countAliveNeighbours(0, 3)) ;
    }

    @Test
    public void whenCheckAliveCellIsAlive_thenTrue() {
        assertTrue(gameOfLife.isAlive(2, 4));
    }

    @Test
    public void whenCheckDeadCellIsAlive_thenFalse() {
        assertFalse(gameOfLife.isAlive(0,0));
    }

    @Test
    public void whenCheckDeadCellIsDead_thenTrue() {
        assertTrue(gameOfLife.isDead(0, 4));
    }

    @Test
    public void whenCheckAliveCellIsDead_thenFalse() {
        assertFalse(gameOfLife.isDead(1,3));
    }

    @Test
    public void whenGridHasOneAliveAndHasLessThanTwoAliveNeighbours_thenCellDies() {
        int[][] localGrid = {
                { 0, 0, 0, 0 },
                { 0, 1, 1, 0 },
                { 0, 0, 0, 0 },
                { 0, 0, 0, 0 },
        };

        int[][] expectedGrid = {
                { 0, 0, 0, 0 },
                { 0, 0, 0, 0 },
                { 0, 0, 0, 0 },
                { 0, 0, 0, 0 },
        };

        GameOfLife gol = new GameOfLife(localGrid.length, localGrid[0].length, localGrid);

        assertTrue(Arrays.deepEquals(expectedGrid, gol.computeNextGeneration()));
    }

    @Test
    public void whenGridHasCellAliveAndHasMoreThanThreeAliveNeighbours_thenCellDies(){
        int[][] localGrid = {
                { 0, 1, 1, 0 },
                { 0, 1, 1, 0 },
                { 0, 0, 1, 0 },
                { 0, 0, 0, 0 },
        };

        int[][] expectedGrid = {
                { 0, 1, 1, 0 },
                { 0, 0, 0, 1 },
                { 0, 1, 1, 0 },
                { 0, 0, 0, 0 }
        };

        GameOfLife gol = new GameOfLife(localGrid.length, localGrid[0].length, localGrid);

        assertTrue(Arrays.deepEquals(expectedGrid, gol.computeNextGeneration()));

    }

    @Test
    public void whenGridHasCellAliveWithTwoOrThreeAliveNeighbours_thenCellLives() {
        int[][] localGrid = {
                { 1, 1, 0, 0 },
                { 0, 1, 0, 0 },
                { 0, 0, 1, 1 },
                { 0, 0, 1, 1 },
        };

        int[][] expectedGrid = {
                { 1, 1, 0, 0 },
                { 1, 1, 0, 0 },
                { 0, 1, 0, 1 },
                { 0, 0, 1, 1 },
        };

        GameOfLife gol = new GameOfLife(localGrid.length, localGrid[0].length, localGrid);

        assertTrue(Arrays.deepEquals(expectedGrid, gol.computeNextGeneration()));

    }

    @Test
    public void whenGridHasDeadCellAndHasExactlyThreeAliveNeighbours_thenBecomesAliveCell() {
        int[][] localGrid = {
                { 0, 1, 0, 0 },
                { 1, 1, 0, 0 },
                { 0, 0, 0, 0 },
                { 0, 0, 0, 0 },
        };

        int[][] expectedGrid = {
                { 1, 1, 0, 0 },
                { 1, 1, 0, 0 },
                { 0, 0, 0, 0 },
                { 0, 0, 0, 0 },
        };

        GameOfLife gol = new GameOfLife(localGrid.length, localGrid[0].length, localGrid);

        assertTrue(Arrays.deepEquals(expectedGrid, gol.computeNextGeneration()));
    }
}
