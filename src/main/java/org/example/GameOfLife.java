package org.example;

public class GameOfLife {

    // Use 0 and 1 instead of . (dot) and * (star) to better identify alive and dead cells
    private final static int DEAD_CELL = 0;
    private final static int LIVE_CELL = 1;

    // Use row and column variable names instead of 'x' and 'y' because that could be confusing
    private final int rowCount;
    private final int columnCount;

    private final int[][] grid;

    public GameOfLife(int rowCount, int columnCount, int[][] grid) {
        this.rowCount = rowCount;
        this.columnCount = columnCount;
        this.grid = grid;
    }

    /**
     * Calculate the next generation of the grid passed in the constructor
     * @return the next generation
     */
    public int[][] computeNextGeneration() {

        int[][] nextGenerationGrid = new int[rowCount][columnCount];

        for (int row = 0; row < rowCount; row++) {
            for (int column = 0; column < columnCount; column++) {

                if (isCellAliveAndHasLessThanTwoLiveNeighbours(row, column)) {
                    nextGenerationGrid[row][column] = DEAD_CELL;
                } else if (isCellAliveAndHasMoreThanThreeLiveNeighbours(row, column)) {
                    nextGenerationGrid[row][column] = DEAD_CELL;
                } else if (isCellAliveAndHasTwoOrThreeLiveNeighbours(row, column)) {
                    nextGenerationGrid[row][column] = LIVE_CELL;
                } else if (isCellDeadAndHasThreeLiveNeighbours(row, column)) {
                    nextGenerationGrid[row][column] = LIVE_CELL;
                } else {
                    nextGenerationGrid[row][column] = grid[row][column];
                }
            }
        }

        return nextGenerationGrid;
    }


    /**
     * From the rules:
     * 1. Any live cell with fewer than two live neighbours dies, as if caused by underpopulation.
     * @param row to check
     * @param column to check
     * @return true if the cell is alive and has less than two live neighbours, otherwise false.
     */
    public boolean isCellAliveAndHasLessThanTwoLiveNeighbours(int row, int column) {
        int neighboursAliveCount = countAliveNeighbours(row, column);
        return isAlive(row, column) && neighboursAliveCount < 2;
    }

    /**
     * From the rules:
     *  2. Any live cell with more than three live neighbours dies, as if by overcrowding.
     * @param row to check
     * @param column to check
     * @return true if the cell is alive and has more than three live neighbours otherwise false.
     */
    public boolean isCellAliveAndHasMoreThanThreeLiveNeighbours(int row, int column) {
        int aliveNeighboursCount = countAliveNeighbours(row, column);
        return isAlive(row, column) && aliveNeighboursCount > 3;
    }

    /**
     * From the rules:
     * 3. Any live cell with two or three live neighbours lives on to the next generation.
     * @param row to check
     * @param column to check
     * @return true if the cell is alive and has two or three living neighbours, otherwise false.
     */
    public boolean isCellAliveAndHasTwoOrThreeLiveNeighbours(int row, int column) {
        int aliveNeighboursCount = countAliveNeighbours(row, column);
        return isAlive(row, column) && (aliveNeighboursCount == 2 || aliveNeighboursCount == 3);
    }

    /**
     * From the rules:
     * 4. Any dead cell with exactly three live neighbours becomes a live cell.
     * @param row to check
     * @param column to check
     * @return true if the cell is dead and has exactly three live neighbours.
     */
    public boolean isCellDeadAndHasThreeLiveNeighbours(int row, int column) {
        int aliveNeighboursCount = countAliveNeighbours(row, column);
        return isDead(row, column) && aliveNeighboursCount == 3;
    }

    /**
     * Count alive neighbours of the given cell
     * The check of neighbors is like following:
     *
     *  grid[row - 1][column - 1] | grid[row - 1][column] |  grid[row - 1][column + 1]
     *  grid[row][column - 1]     |          x            |  grid[row][column + 1]
     *  grid[row + 1][column - 1] | grid[row + 1][column] |  grid[row + 1][column + 1]
     *
     * Careful to go out boundaries!
     *
     * @param row to check
     * @param column to check
     * @return number of alive neighbours of the current cell.
     */
    public int countAliveNeighbours(int row, int column) {

        int aliveNeighbours = 0;
        int[][] cellsToCheck = {
                {row - 1, column - 1},
                {row - 1, column},
                {row - 1, column + 1},
                {row, column + 1},
                {row + 1, column + 1},
                {row + 1, column},
                {row + 1, column - 1},
                {row, column - 1},
        };

        for(int i = 0; i < cellsToCheck.length; i++) {
            int rowToCheck = cellsToCheck[i][0];
            int columnToCheck = cellsToCheck[i][1];

            if(isInTheGrid(rowToCheck, columnToCheck) && isAlive(rowToCheck, columnToCheck)) {
                aliveNeighbours++;
            }
        }

        return aliveNeighbours;
    }

    /**
     * Check if current cell is not out boundaries
     * @param row to check
     * @param column to check
     * @return whether the cell to check is inside the grid boundaries.
     */
    private boolean isInTheGrid(int row, int column) {
        return row >= 0 && column >= 0 && row < rowCount && column < columnCount;
    }

    /**
     * Check if the cell is alive
     * @param row to check
     * @param column to check
     * @return true if the cell is alive otherwise false
     */
    public boolean isAlive(int row, int column) {
        return grid[row][column] == LIVE_CELL;
    }

    /**
     * Check if the cell is dead
     * @param row to check
     * @param column to check
     * @return true if the cell is dead otherwise false
     */
    public boolean isDead(int row, int column) {
        return grid[row][column] == DEAD_CELL;
    }

}
