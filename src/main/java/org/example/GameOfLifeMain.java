package org.example;

public class GameOfLifeMain {

    public static void main (String[] args) {
        // Grid to test from instructions, this could be read from file or another input method
        // doing this way for simplicity.
        int[][] grid = {
                { 0, 0, 0, 0, 0, 0, 0, 0 },
                { 0, 0, 0, 0, 1, 0, 0, 0 },
                { 0, 0, 0, 1, 1, 0, 0, 0 },
                { 0, 0, 0, 0, 0, 0, 0, 0 }
        };

        printGrid(grid);
        GameOfLife gameOfLife = new GameOfLife(grid.length, grid[0].length, grid);
        System.out.println("Next Generation");
        int[][] result = gameOfLife.computeNextGeneration();

        printGrid(result);
    }

    private static void printGrid(int[][] grid) {
        for(int row = 0; row < grid.length; row++){
            for (int column = 0; column < grid[0].length; column++){
                System.out.print(grid[row][column]);
            }
            System.out.println("");
        }
    }
}
